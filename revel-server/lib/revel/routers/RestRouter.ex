defmodule Revel.RestRouter do
  require Logger
  use Plug.Router
  import XmlBuilder
  import Revel.Common

  plug(:match)
  plug(:dispatch)

  @moduledoc """
  Implements a subset of the Subsonic api (version 1.15.0)
  Only supports XML
  API spec available at http://www.subsonic.org/pages/api.jsp
  """

  match "/ping.view" do
    conn = Plug.Conn.fetch_query_params(conn)

    case conn.assigns.format do
      :xml ->
        resp =
          element(:"subsonic-response", %{
            xmlns: "http://subsonic.org/restapi",
            status: "ok",
            version: "1.15.0"
          })

        xml_resp = resp |> XmlBuilder.generate()

        send_xml(conn, xml_resp)

      :json ->
        resp =
          Jason.encode!(%{
            "subsonic-response" => %{
              "status" => "ok",
              "version" => "1.15.0"
            }
          })

        send_json(conn, resp)
    end
  end

  # Dummy endpoint to prevent errors
  match "/getArtistInfo.view" do
    conn |> send_resp(200, "")
  end

  # Returns valid license (not sure if this is sufficient)
  match "/getLicense.view" do
    resp =
      element(
        :"subsonic-response",
        %{xmlns: "http://subsonic.org/restapi", status: "ok", version: "1.15.0"},
        [
          element(:license, %{
            value: "true",
            email: "foo@bar.com",
            licenseExpires: "2019-09-03T14:46:43"
          })
        ]
      )

    xml_resp = resp |> XmlBuilder.generate()
    send_xml(conn, xml_resp)
  end

  # Dummy endpoint to prevent errors
  match "/getMusicFolders.view" do
    resp =
      element(
        :"subsonic-response",
        %{xmlns: "http://subsonic.org/restapi", status: "ok", version: "1.15.0"},
        [element(:musicFolders, %{}, [element(:musicFolder, %{id: "1", name: "ohea"})])]
      )

    xml_resp = resp |> XmlBuilder.generate()
    send_xml(conn, xml_resp)
  end

  # Get indexes returns the contents of the root directory
  # This directory should only contain folders
  match "/getIndexes.view" do

    # Get db entry for requesting user
    [user] = Revel.Database.get_user(conn.query_params["u"])

    # Get all folders that are at the root level
    folders = Revel.Database.get_folders_with_root(user[:root])

    case conn.assigns.format do
      :xml ->
        # Convert database query to a xml map
        artist_map =
          folders
            |> Enum.map(fn [
                             id: _,
                             parent: _,
                             type: _,
                             filepath: _,
                             id: id,
                             filename: filename
                           ] ->
              element(:artist, %{id: id, name: filename}, [])
            end)
        
        # Convert the database query into a xml document
        resp =
          element(
            :"subsonic-response",
            %{xmlns: "http://subsonic.org/restapi", status: "ok", version: "1.15.0"},
            [
              element(:indexes, %{}, [
                element(
                  :index,
                  %{name: "A"},
                  # all elements are listed as if they began with A
                  # This works fine in dsub (I have not tested other clients)
                  artist_map
                )
              ])
            ]
          )

        xml_resp = resp |> XmlBuilder.generate()
        send_xml(conn, xml_resp)

      :json ->
        # Convert the database query to a json map
        artist_map =
          folders
          |> Enum.map(fn [id: _, parent: _, type: _, filepath: _, id: id, filename: filename] ->
            %{
              "id" => id,
              "name" => filename
            }
          end)

        # Wrap the database query in the subsonic json format
        resp =
          Jason.encode!(%{
            "subsonic-response" => %{
              "status" => "ok",
              "version" => "1.15.0",
              "indexes" => %{
                "index" => [
                  %{
                    "name" => "A",
                    "artist" => artist_map
                  }
                ]
              }
            }
          })

        send_json(conn, resp)
    end
  end


  match "/getArtists.view" do
    # Get all available artists
    folders = Revel.Database.get_folders_with_root(0)

    case conn.assigns.format do
      :xml ->
        # Convert the database query into a xml document
        resp =
          element(
            :"subsonic-response",
            %{xmlns: "http://subsonic.org/restapi", status: "ok", version: "1.15.0"},
            [
              element(:indexes, %{}, [
                element(
                  :index,
                  %{name: "A"},
                  # all elements are listed as if they began with A
                  # This works fine in dsub (I have not tested other clients)
                  folders
                  |> Enum.map(fn [
                                   id: _,
                                   parent: _,
                                   type: _,
                                   filepath: _,
                                   id: id,
                                   filename: filename
                                 ] ->
                    element(:artist, %{id: id, name: filename}, [])
                  end)
                )
              ])
            ]
          )

        xml_resp = resp |> XmlBuilder.generate()
        send_xml(conn, xml_resp)

      :json ->
        # Convert the database query to a json map
        artist_map =
          folders
          |> Enum.map(fn [id: _, parent: _, type: _, filepath: _, id: id, filename: filename] ->
            %{
              "id" => id,
              "name" => filename
            }
          end)

        # Wrap the database query in the subsonic json format
        resp =
          Jason.encode!(%{
            "subsonic-response" => %{
              "status" => "ok",
              "version" => "1.15.0",
              "indexes" => %{
                "index" => [
                  %{
                    "name" => "A",
                    "artist" => artist_map
                  }
                ]
              }
            }
          })

        send_json(conn, resp)
    end
  end
  
  
  # Return the files and folders contained in the specified folder
  match "/getMusicDirectory.view" do
    conn = Plug.Conn.fetch_query_params(conn)

    # check if the user has provided an id
    case check_map_for_keys(conn.query_params, ["id"]) do
      true ->
        # query the database for the requested folder (conn.query_params["id"])
        [folder] = Revel.Database.get_folders_by_id(conn.query_params["id"])

        # query the database for the subfolders of the requested folder
        subfolders = Revel.Database.get_folders_by_parent_id(folder[:id])

        # query the database for the files inside the requested folder
        subfiles = Revel.Database.get_files_by_parent_id(folder[:id])

        case conn.assigns.format do
          :xml ->
            # convert the files database query into an array of xml objects
            files_xml =
              subfiles
              |> Enum.map(fn [
                               id: id,
                               parent: parent,
                               type: _,
                               filepath: _,
                               id: _,
                               title: title,
                               artist: artist,
                               trackn: trackn,
                               year: year,
                               duration: duration,
                               genre: genre,
                               ext: ext,
                               filename: _
                             ] ->
                element(
                  :child,
                  %{
                    id: id,
                    parent: parent,
                    title: title,
                    artist: artist,
                    track: trackn,
                    year: year,
                    genre: genre,
                    duration: duration,
                    suffix: ext,
                    isDir: "false",
                    coverArt: 0,
                    transcodedContentType: "audio/mpeg",
                    transcodedSuffix: "mp3",
                    bitRate: "320"
                  },
                  []
                )
              end)

            # convert the folders database query into an array of xml objects
            folders_xml =
              subfolders
              |> Enum.map(fn [
                               id: id,
                               parent: parent,
                               type: _,
                               filepath: _,
                               id: _,
                               filename: name
                             ] ->
                element(
                  :child,
                  %{
                    id: id,
                    parent: parent,
                    title: name,
                    artist: folder[:filename],
                    isDir: "true",
                    coverArt: 0
                  },
                  []
                )
              end)

            # append the files and folders xml arrays to each other
            files_and_folders_xml = folders_xml ++ files_xml

            # generate a response element that contains the appended arrays
            resp =
              element(
                :"subsonic-response",
                %{xmlns: "http://subsonic.org/restapi", status: "ok", version: "1.15.0"},
                [
                  element(
                    :directory,
                    %{id: folder[:id], parent: folder[:parent], name: folder[:filename]},
                    files_and_folders_xml
                  )
                ]
              )

            # serialize xml into text
            xml_resp = resp |> XmlBuilder.generate()

            send_xml(conn, xml_resp)

          :json ->
            # convert the files database query into an array of xml objects
            files_json =
              subfiles
              |> Enum.map(fn [
                               id: id,
                               parent: parent,
                               type: _,
                               filepath: path,
                               id: _,
                               title: title,
                               artist: artist,
                               trackn: trackn,
                               year: year,
                               duration: duration,
                               genre: genre,
                               ext: ext,
                               filename: _
                             ] ->
                %{
                  id: to_string(id),
                  parent: to_string(parent),
                  title: title,
                  artist: artist,
                  track: trackn,
                  year: year,
                  genre: genre,
                  duration: duration,
                  path: path,
                  suffix: ext,
                  isDir: false,
                  coverArt: 0,
                  transcodedContentType: "audio/mpeg",
                  transcodedSuffix: "mp3",
                  bitRate: "320",
                  playCount: 0
                }
              end)

            # convert the folders database query into an array of xml objects
            folders_json =
              subfolders
              |> Enum.map(fn [
                               id: id,
                               parent: parent,
                               type: _,
                               filepath: _,
                               id: _,
                               filename: name
                             ] ->
                %{
                  id: to_string(id),
                  parent: to_string(parent),
                  title: name,
                  artist: folder[:filename],
                  isDir: true,
                  coverArt: 0,
                  playCount: 0
                }
              end)

            # append the files and folders json to each other
            files_and_folders_json = folders_json ++ files_json

            # generate a response element that contains the appended arrays
            resp =
              Jason.encode!(%{
                "subsonic-response" => %{
                  status: "ok",
                  version: "1.15.0",
                  directory: %{
                    id: to_string(folder[:id]),
                    parent: to_string(folder[:parent]),
                    name: folder[:filename],
                    playCount: 0,
                    child: files_and_folders_json
                  }
                }
              })

            send_json(conn, resp)
        end

      false ->
        send_error(conn, 10)
    end
  end

  # Return the requested file
  # The file is ether the direct file from disk or
  # transcoded to the desired bitrate
  match "stream.view" do
    conn = Plug.Conn.fetch_query_params(conn)

    case check_map_for_keys(conn.query_params, ["id"]) do
      false ->
        send_error(conn, 10)

      true ->
        # WIP implementation of transcoding
        # Need to make support for potential parameters more robust
        case check_map_for_keys(conn.query_params, ["maxBitRate"]) do
          true ->
            send_transcoded_file(conn)

          false ->
            send_file(conn)
        end
    end
  end

  match "getSong.view" do
    conn = Plug.Conn.fetch_query_params(conn)

    case check_map_for_keys(conn.query_params, ["id"]) do
      false ->
        send_error(conn, 10)

      true ->
        [file] = Revel.Database.get_files_by_id(conn.query_params["id"])

        case conn.assigns.format do
          :xml ->
            # generate a response element that contains the appended arrays
            resp =
              element(
                :"subsonic-response",
                %{xmlns: "http://subsonic.org/restapi", status: "ok", version: "1.15.0"},
                [
                  element(
                    :song,
                    %{
                      id: file[:id],
                      parent: file[:parent],
                      isDir: "false",
                      title: file[:title],
                      artist: file[:artist],
                      track: file[:trackn],
                      year: file[:year],
                      genre: file[:genre],
                      suffix: file[:ext],
                      duration: file[:duration],
                      path: file[:filepath]
                    }
                  )
                ]
              )

            # serialize xml into text
            xml_resp = resp |> XmlBuilder.generate()

            send_xml(conn, xml_resp)

          :json ->
            resp =
              Jason.encode!(%{
                "subsonic-response" => %{
                  status: "ok",
                  version: "1.15.0",
                  song: %{
                    id: to_string(file[:id]),
                    parent: to_string(file[:parent]),
                    isDir: false,
                    title: file[:title],
                    artist: file[:artist],
                    track: file[:trackn],
                    year: file[:year],
                    genre: file[:genre],
                    suffix: file[:ext],
                    duration: file[:duration],
                    path: file[:filepath]
                  }
                }
              })

            send_json(conn, resp)
        end
    end
  end

  match "getCoverArt.view" do
    conn
    |> send_resp(404, "Not Found")
    #conn
    #|> send_file(200, "/home/robby/Pictures/test.png")
  end

  match "updateUser.view" do
    case conn.assigns[:admin] do
      true ->
        conn = Plug.Conn.fetch_query_params(conn)

        case check_map_for_keys(conn.query_params, ["username"]) do
          false ->
            send_error(conn, 10)

          true ->
            username = conn.query_params["username"]
            [user] = Revel.Database.get_user(username)

            password =
              case check_map_for_keys(conn.query_params, ["password"]) do
                true ->
                  conn.query_params["password"]

                false ->
                  user[:password]
              end

            admin_role =
              convert_bool_literal_from_map_to_int(
                conn.query_params,
                "adminRole",
                user[:admin_role]
              )

            settings_role =
              convert_bool_literal_from_map_to_int(
                conn.query_params,
                "settingsRole",
                user[:settings_role]
              )

            download_role =
              convert_bool_literal_from_map_to_int(
                conn.query_params,
                "downloadRole",
                user[:download_role]
              )

            playlist_role =
              convert_bool_literal_from_map_to_int(
                conn.query_params,
                "playlistRole",
                user[:playlist_role]
              )

            cover_art_role =
              convert_bool_literal_from_map_to_int(
                conn.query_params,
                "coverArtRole",
                user[:cover_art_role]
              )

            case Revel.Database.update_user(username, %{
                   :password => password,
                   :admin_role => admin_role,
                   :settings_role => settings_role,
                   :download_role => download_role,
                   :playlist_role => playlist_role,
                   :cover_art_role => cover_art_role
                 }) do
              :ok ->
                case conn.assign.format do
                  :xml ->
                    resp =
                      element(
                        :"subsonic-response",
                        %{xmlns: "http://subsonic.org/restapi", status: "ok", version: "1.15.0"},
                        []
                      )

                    xml_resp = resp |> XmlBuilder.generate()
                    send_xml(conn, xml_resp)

                  :json ->
                    resp =
                      Jason.encode!(%{
                        "subsonic-response" => %{
                          "status" => "ok",
                          "version" => "1.15.0"
                        }
                      })

                    send_json(conn, resp)
                end

              :error ->
                send_error(conn, 0)
            end
        end

      false ->
        send_error(conn, 50)
    end
  end

  match "createUser.view" do
    case conn.assigns[:admin] do
      true ->
        conn = Plug.Conn.fetch_query_params(conn)

        case check_map_for_keys(conn.query_params, ["username", "password"]) do
          false ->
            send_error(conn, 10)

          true ->
            username = conn.query_params["username"]
            password = conn.query_params["password"]
            admin_role = convert_bool_literal_from_map_to_int(conn.query_params, "adminRole", 0)

            settings_role =
              convert_bool_literal_from_map_to_int(conn.query_params, "settingsRole", 1)

            download_role =
              convert_bool_literal_from_map_to_int(conn.query_params, "downloadRole", 0)

            playlist_role =
              convert_bool_literal_from_map_to_int(conn.query_params, "playlistRole", 0)

            cover_art_role =
              convert_bool_literal_from_map_to_int(conn.query_params, "coverArtRole", 0)

            case Revel.Database.create_user(username, %{
                   :password => password,
                   :admin_role => admin_role,
                   :settings_role => settings_role,
                   :download_role => download_role,
                   :playlist_role => playlist_role,
                   :cover_art_role => cover_art_role
                 }) do
              :ok ->
                case conn.assign.format do
                  :xml ->
                    resp =
                      element(
                        :"subsonic-response",
                        %{xmlns: "http://subsonic.org/restapi", status: "ok", version: "1.15.0"},
                        []
                      )

                    xml_resp = resp |> XmlBuilder.generate()
                    send_xml(conn, xml_resp)

                  :json ->
                    resp =
                      Jason.encode!(%{
                        "subsonic-response" => %{
                          "status" => "ok",
                          "version" => "1.15.0"
                        }
                      })

                    send_json(conn, resp)
                end

              :error ->
                send_error(conn, 0)
            end
        end

      false ->
        send_error(conn, 50)
    end
  end

  match "deleteUser.view" do
    case conn.assigns[:admin] do
      false ->
        send_error(conn, 50)

      true ->
        conn = Plug.Conn.fetch_query_params(conn)

        case check_map_for_keys(conn.query_params, ["username"]) do
          false ->
            send_error(conn, 10)

          true ->
            username = conn.query_params["username"]

            case Revel.Database.delete_user(username) do
              :error ->
                send_error(conn, 0)

              :ok ->
                case conn.assign.format do
                  :xml ->
                    resp =
                      element(
                        :"subsonic-response",
                        %{xmlns: "http://subsonic.org/restapi", status: "ok", version: "1.15.0"},
                        []
                      )

                    xml_resp = resp |> XmlBuilder.generate()
                    send_xml(conn, xml_resp)

                  :json ->
                    resp =
                      Jason.encode!(%{
                        "subsonic-response" => %{
                          "status" => "ok",
                          "version" => "1.15.0"
                        }
                      })

                    send_json(conn, resp)
                end
            end
        end
    end
  end

  # Generic match for unsupported endpoints
  match _ do
    send_resp(conn, 404, "Not Found")
  end
end
