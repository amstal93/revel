pub mod db_operations;
mod utilities;
mod thread_functions;
mod structs;

use structs::FileSystemEntry;

use failure::Error;

pub fn scan_and_populate_database(db_conn: rusqlite::Connection, scan_path: &str, num_threads: usize) -> Result<(), Error> {

    // declare thread communication channels
    let (mut to_scan_tx, to_scan_rx) = spmc::channel::<FileSystemEntry>();
    let (to_insert_tx, to_insert_rx) = crossbeam_channel::unbounded();

    // use rayon::scope to allow main thread to wait for completion
    rayon::scope(|s| {
        // init num_threads scanner threads
        for _ in 0..(num_threads) {
            let rx = to_scan_rx.clone();
            let tx = to_insert_tx.clone();
            s.spawn(move |_| thread_functions::tag_parser_thread(tx, rx));
        }

        // setup database writer thread
        {
            s.spawn(move |_| thread_functions::database_insertion_thread(to_insert_rx, db_conn));
        }

        // drop references to channels that main thread doesn't need
        drop(to_scan_rx);
        drop(to_insert_tx);

        let music_directory = String::from(scan_path);
        utilities::scan_directory_and_send_to_channel(music_directory, &mut to_scan_tx).unwrap();
        drop(to_scan_tx);
    });
    Ok(())
}

pub fn rescan_and_update_database(db_conn: rusqlite::Connection, scan_path: &str, num_threads: usize) -> Result<(), Error> {


    let db_entries = db_operations::create_HashSet_from_database_entries(&db_conn).unwrap();

    // declare thread communication channels
    let (mut to_scan_tx, to_scan_rx) = spmc::channel::<FileSystemEntry>();
    let (to_insert_tx, to_insert_rx) = crossbeam_channel::unbounded();

    // use rayon::scope to allow main thread to wait for completion
    rayon::scope(|s| {
        // init num_threads scanner threads
        for _ in 0..(num_threads) {
            let rx = to_scan_rx.clone();
            let tx = to_insert_tx.clone();
            s.spawn(move |_| thread_functions::tag_parser_thread(tx, rx));
        }

        // setup database writer thread
        {
            s.spawn(move |_| thread_functions::database_insertion_thread(to_insert_rx, db_conn));
        }

        // drop references to channels that main thread doesn't need
        drop(to_scan_rx);
        drop(to_insert_tx);

        let music_directory = String::from(scan_path);
        utilities::rescan_directory_and_send_to_channel(music_directory, &mut to_scan_tx, db_entries).unwrap();
        drop(to_scan_tx);
    });
    Ok(())
}
