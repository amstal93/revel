#![allow(non_snake_case)]

use std::hash::{Hash, Hasher};
use twox_hash;

// struct to hold entries from walkdir
pub struct FileSystemEntry {
    pub id: i64,
    pub parent_id: i64,
    pub path: String,
    pub file_name: String,
    pub extension: String,
    pub file_type: std::fs::FileType,
}

// struct to hold entries for database insertion
pub struct DatabaseEntry {
    pub fs_entry: FileSystemEntry,
    pub title: String,
    pub artist: String,
    pub album: String,
    pub year: u32,
    pub track: u32,
    pub genre: String,
}

pub fn create_FileSystemEntry_from_DirEntry(
    dir_entry: walkdir::DirEntry) -> Result<FileSystemEntry, failure::Error> {
    
    // use walkdir's provided FileType
    let file_type = dir_entry.file_type();

    // take the 64 bit xxhash of the file/folder path
    let path = dir_entry.path().to_str().unwrap().to_string();
    let mut hasher = twox_hash::XxHash64::with_seed(69);
    path.hash(&mut hasher);
    let id = hasher.finish() as i64;

    // take the 64 bit xxhash of the parent folder's path
    let parent = dir_entry.path().parent().unwrap();
    let parent_path = parent.to_str().unwrap().to_string();
    let mut hasher = twox_hash::XxHash64::with_seed(69);
    parent_path.hash(&mut hasher);
    let parent_id = hasher.finish() as i64;

    let file_name = dir_entry.path().file_name().unwrap().to_str().unwrap().to_string();
    
    let extension: String;
    
    if file_type.is_file() {
        extension = dir_entry
                        .path()
                        .extension()
                        .unwrap()
                        .to_str()
                        .unwrap()
                        .to_string();
    }
    else {
        extension = "".to_string();
    }

    // store all data in struct and return
    Ok(FileSystemEntry {
        id,
        parent_id,
        path,
        file_name,
        extension,
        file_type,
    })
}


pub fn scan_FileSystemEntry_into_DatabaseEntry(fs_entry: FileSystemEntry) -> Result<DatabaseEntry, failure::Error> {
    if fs_entry.file_type.is_file() {
        let file = taglib::File::new(&fs_entry.path).unwrap();
        // TODO: improve taglib error handling
        //.map_err(|e| format_err!("file error: {:?}", e))?;
        let tag = file.tag().unwrap();

        let title = tag.title().unwrap_or_default();
        let artist = tag.artist().unwrap_or_default();
        let album = tag.album().unwrap_or_default();
        let year = tag.year().unwrap_or_default();
        let track = tag.track().unwrap_or_default();
        let genre = tag.genre().unwrap_or_default();

        Ok(DatabaseEntry {
            fs_entry,
            title,
            artist,
            album,
            year,
            track,
            genre,
        })
    } else if fs_entry.file_type.is_dir() {
        Ok(DatabaseEntry {
            title: fs_entry.file_name.clone(),
            fs_entry,
            artist: "".to_string(),
            album: "".to_string(),
            year: 0,
            track: 0,
            genre: "".to_string(),
        })
    } else {
        Err(failure::err_msg("Unknown file type"))
    }
}
