mod utils;
use utils::db_operations;

use rustler::{Encoder, Env, Error, Term};

mod atoms {
    rustler::rustler_atoms! {
        atom ok;
        atom error;
        //atom __true__ = "true";
        //atom __false__ = "false";
    }
}

rustler::rustler_export_nifs! {
    "Elixir.Revel.Scanner",
    [
        ("update_database", 3, update_database),
        ("create_database", 3, create_database)
    ],
    None
}


fn update_database<'a>(env: Env<'a>, args: &[Term<'a>]) -> Result<Term<'a>, Error> {
    let db_path: &str = args[0].decode()?;
    let scan_path: &str = args[1].decode()?;
    let num_threads: usize = args[2].decode()?;

    // verify that database exists
    if !std::path::Path::new(db_path).exists() {
        return Err(rustler::error::Error::Atom("db_does_not_exist"));
    }

    let conn = rusqlite::Connection::open(&db_path).unwrap();
    utils::rescan_and_update_database(conn, scan_path, num_threads).unwrap();
    Ok(atoms::ok().encode(env))
}


fn create_database<'a>(env: Env<'a>, args: &[Term<'a>]) -> Result<Term<'a>, Error> {
    let db_path: &str = args[0].decode()?;
    let scan_path: &str = args[1].decode()?;
    let num_threads: usize = args[2].decode()?;

    // error if database exists
    if std::path::Path::new(db_path).exists() {
        return Err(rustler::error::Error::Atom("db_exists"));
    }

    let conn = rusqlite::Connection::open(&db_path).unwrap();
    db_operations::init_database(&conn, scan_path).unwrap();

    utils::scan_and_populate_database(conn, scan_path, num_threads).unwrap();

    Ok(atoms::ok().encode(env))
}

