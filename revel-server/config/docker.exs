use Mix.Config

config :logger,
  backends: [:console],
  compile_time_purge_level: :info

config :logger, :console,
  level: :info,
  metedata: :all

config :revel,
  default_config:
      """
      [Paths]
      # Paths ##########################################################
      #
      # This directory is where revel will index and serve music from.
      #
      music_path = /music
      #
      # This directory sets the location where revel will temporaily
      # store converted files
      # If unset, will default to $XDG_CACHE_HOME
      tmp_dir =

      [Network]
      # Network ########################################################
      #
      listen_ip = 0.0.0.0
      listen_port = 32320
      # This is the web path that the revel server will be accessed at.
      # If your server is available at http://example.com/revel, path
      # should be set to /revel
      path = /
      """
