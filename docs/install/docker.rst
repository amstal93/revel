Installing Revel Using Docker
=============================

Download the Container
----------------------

There are two versions of Revel that are available via docker. The
``revel:stable`` tag tracks the latest tagged release. The ``revel:dev`` tag
tracks the HEAD of the master branch.

To pull the stable version run the following::

    $ docker pull registry.gitlab.com/robozman/revel/revel:stable

To pull the dev version run the following::

    $ docker pull registry.gitlab.com/robozman/revel/revel:dev

Create Volumes
--------------

Revel uses docker volumes to store the config file and database. This allows
these files to persist through restarts and upgrades.

To create the two volumes run::

        $ docker volume create revel-db
        $ docker volume create revel-config

.. note::

    If you start the container without creating these volumes, it will
    automatically create two equivlent volumes. These volumes will have
    randomly generated names which can be annoying when you want to upgrade the
    container.

Run the Container
-----------------

Now that we have the container downloaded, let's start it. We will use the
``docker run`` command to do this.

We will pass the following parameters to the ``docker run`` command.

    - ``-d``:  Run the container detached from the active console.
    - ``--name revel``: Give the container a easy name to reference later.
    - ``--restart unless-stopped``: Set the container to restart automatically
      unless explicitly stopped.
    - ``-v /path/to/music/on/host:/music:ro``: Map the ``/music`` directory
      in the container to the location of your music on the host machine. Fill in your music directory here.
    - ``-v revel-db:/revel/.local/share/revel``: Maps the ``revel-db`` volume to the database directory.
    - ``-v revel-config:/revel/.config/revel``: Maps the ``revel-config`` volume to the config directory.
    - ``-e REVEL_UID=YOUR_UID -e REVEL_GID=YOUR_GID``: Sets the user and group revel will run as. Set this to something other then root.

To start the stable version run the following::

        $ docker run -d --name "revel" --restart unless-stopped \
            -v /path/to/music/on/host:/music:ro \
            -v revel-config:/revel/.config/revel \
            -v revel-db:/revel/.local/share/revel \
            -e REVEL_UID=YOUR_UID -e REVEL_GID=YOUR_GID \
            registry.gitlab.com/robozman/revel/revel:stable

To start the dev version run the following::

        $ docker run -d --name "revel" --restart unless-stopped \
            -v /path/to/music/on/host:/music:ro \
            -v revel-config:/revel/.config/revel \
            -v revel-db:/revel/.local/share/revel \
            -e REVEL_UID=YOUR_UID -e REVEL_GID=YOUR_GID \
            registry.gitlab.com/robozman/revel/revel:dev

Revel will now start and begin scanning your filesystem. This may take some
time. To view Revel's log and check startup status run::

        $ docker logs revel

Get the Container's IP Address
------------------------------

Docker will assign a random IP address to the container. To find out this this
IP address run the following::

        $ docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' revel

Revel will now be available on port 32320 at this IP address.

Updating the Container
----------------------

First, stop the running container::

        $ docker stop revel


Next, pull the latest version of the container::

        stable version
        $ docker pull registry.gitlab.com/robozman/revel/revel:stable

        dev version
        $ docker pull registry.gitlab.com/robozman/revel/revel:dev


Finally, start the container again::

        stable version
        $ docker run -d --name "revel" --restart unless-stopped \
            -v /path/to/music/on/host:/music:ro \
            -v revel-config:/revel/.config/revel \
            -v revel-db:/revel/.local/share/revel \
            -e REVEL_UID=YOUR_UID -e REVEL_GID=YOUR_GID \
            registry.gitlab.com/robozman/revel/revel:stable

        dev version
        $ docker run -d --name "revel" --restart unless-stopped \
            -v /path/to/music/on/host:/music:ro \
            -v revel-config:/revel/.config/revel \
            -v revel-db:/revel/.local/share/revel \
            -e REVEL_UID=YOUR_UID -e REVEL_GID=YOUR_GID \
            registry.gitlab.com/robozman/revel/revel:dev

