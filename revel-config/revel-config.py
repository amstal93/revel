#!/usr/bin/python3

import argparse
import hashlib
import random
import requests
import string
import urllib.parse

def str2bool(v):
    if isinstance(v, bool):
       return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

def int_to_bool_str(i):
    if i > 0:
        return "True"
    else:
        return "False"

class RevelServer():
    def __init__(self, url, username, password, subsonic_api_version):
        if url[len(url)-1] != "/":
            self.url = url + "/"
        else:
            self.url = url

        self.username = username
        self.password = password
        self.salt = ''.join(random.choices(string.ascii_uppercase + string.digits, k=20))
        salted_hash = hashlib.md5(self.password.encode('ascii') + self.salt.encode('ascii'))
        self.salt_hash_string = salted_hash.hexdigest()

        self.subsonic_api_version = subsonic_api_version

    def parse_and_run(self, args):
        method_name = args.endpoint_name
        method = getattr(self, method_name)

        return method(args)

    def ping(self, args):
        payload = {'u': self.username, 't': self.salt_hash_string,
                's': self.salt, 'v': self.subsonic_api_version,
                'c': 'revel-config'}
        full_url = urllib.parse.urljoin(self.url, './rest/ping.view')
        r = requests.get(full_url, params=payload)
        print(r.text)
        return

    def getUser(self, args):
        return

    def getUsers(self, args):
        return

    def createUser(self, args):
        createUser_payload = {'u': self.username, 't': self.salt_hash_string,
                's': self.salt, 'v': self.subsonic_api_version,
                'c': 'revel-config', 'username': args.target_username,
                'password': args.target_password}
        try:
            createUser_payload['adminRole'] = args.adminRole
        except AttributeError:
            pass
        try:
            createUser_payload['settingsRole'] = args.settingsRole
        except AttributeError:
            pass
        try:
            createUser_payload['downloadRole'] = args.downloadRole
        except AttributeError:
            pass
        try:
            createUser_payload['covertArtRole'] = args.coverArtRole
        except AttributeError:
            pass


        full_url = urllib.parse.urljoin(self.url, './rest/createUser.view')
        r = requests.get(full_url, params=createUser_payload)
        print(r.text)
        return

    def updateUser(self, args):
        updateUser_payload = {'u': self.username, 't': self.salt_hash_string,
                's': self.salt, 'v': self.subsonic_api_version,
                'c': 'revel-config', 'username': args.target_username}
        try:
            updateUser_payload['password'] = args.target_password
        except AttributeError:
            pass
        try:
            updateUser_payload['adminRole'] = args.adminRole
        except AttributeError:
            pass
        try:
            updateUser_payload['settingsRole'] = args.settingsRole
        except AttributeError:
            pass
        try:
            updateUser_payload['downloadRole'] = args.downloadRole
        except AttributeError:
            pass
        try:
            updateUser_payload['covertArtRole'] = args.coverArtRole
        except AttributeError:
            pass

        full_url = urllib.parse.urljoin(self.url, './rest/updateUser.view')
        r = requests.get(full_url, params=updateUser_payload)
        print(r.text)
        return

    def deleteUser(self, args):
        deleteUser_payload = {'u': self.username, 't': self.salt_hash_string,
                's': self.salt, 'v': self.subsonic_api_version,
                'c': 'revel-config', 'username': args.target_username}

        full_url = urllib.parse.urljoin(self.url, './rest/deleteUser.view')
        r = requests.get(full_url, params=deleteUser_payload)
        print(r.text)
        return

def create_parser():
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(help='sub-command help',
                    dest='endpoint_name')
    
    ping_parser = subparsers.add_parser('ping',
                    help='pings the server to check if it is online')


    getUser_parser = subparsers.add_parser('getUser',
                    help='Gets details about the requested user')
    getUser_parser.add_argument('target_username', type=str,
                    help='Username of requested user')

    getUsers_parser = subparsers.add_parser('getUsers',
                    help='Get detals about all users')
   


    createUser_parser = subparsers.add_parser('createUser',
                    help='Creates a new Subsonic user')
    createUser_parser.add_argument('target_username', type=str,
                    help='Username of user')
    createUser_parser.add_argument('target_password', type=str,
                    help='Password of user')
    createUser_parser.add_argument('--adminRole', type=str2bool,
                    help='')
    createUser_parser.add_argument('--settingsRole', type=str2bool,
                    help='')
    createUser_parser.add_argument('--downloadRole', type=str2bool,
                    help='')
    createUser_parser.add_argument('--playlistRole', type=str2bool,
                    help='')
    createUser_parser.add_argument('--coverArtRole', type=str2bool,
                    help='')

    updateUser_parser = subparsers.add_parser('updateUser',
              help='Creates a new Subsonic user')
    updateUser_parser.add_argument('target_username', type=str,
              help='Username of user')
    updateUser_parser.add_argument('--target_password', type=str,
              help='new password for user')
    updateUser_parser.add_argument('--adminRole', type=str2bool,
              help='')
    updateUser_parser.add_argument('--settingsRole', type=str2bool,
              help='')
    updateUser_parser.add_argument('--downloadRole', type=str2bool,
              help='')
    updateUser_parser.add_argument('--playlistRole', type=str2bool,
              help='')
    updateUser_parser.add_argument('--coverArtRole', type=str2bool,
                    help='')

    deleteUser_parser = subparsers.add_parser('deleteUser',
              help='Deletes a Subsonic User')
    deleteUser_parser.add_argument('target_username', type=str,
              help='Username of user')


    parser.add_argument('--url', metavar='url', type=str,
                    default='http://localhost:32320/',
                    help='url of the desired server')
    parser.add_argument('--username', metavar='username', type=str,
                    default='admin',
                    help='username to authenticate with')
    parser.add_argument('--password', metavar='password', type=str,
                    default='password',
                    help='password to authenticate with')

    return parser


def main():

    REVEL_CONFIG_VERSION = '0.7.0'

    SUBSONIC_API_VERSION = '1.15.0'

    print('revel-config Version:', REVEL_CONFIG_VERSION)
    print('Subsonic API Version:', SUBSONIC_API_VERSION)

    parser = create_parser()

    args = parser.parse_args()

    server = RevelServer(args.url, args.username, args.password, SUBSONIC_API_VERSION)
    server.parse_and_run(args)
    




if __name__ == "__main__":
    main()
