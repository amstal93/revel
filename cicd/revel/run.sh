#!/bin/bash

chown -R "$REVEL_UID:$REVEL_GID" /revel
su-exec "$REVEL_UID:$REVEL_GID" /revel/bin/revel foreground
